const mongoose = require('mongoose');

const groupSchema = new mongoose.Schema({
    name: String,
    description: String,
    isactive:Boolean,
    Categories : [
      {type: mongoose.Schema.Types.ObjectId,ref:'Categories'}
  ]
   
});

const Group = mongoose.model('Groups', groupSchema);

module.exports = Group;
